sap.ui.jsview("EnYapp.view.App",
		{

			/**
			 * Specifies the Controller belonging to this View. In the case that
			 * it is not implemented, or that "null" is returned, this View does
			 * not have a Controller.
			 * 
			 * @memberOf view.App
			 */
			getControllerName : function() {
				return "EnYapp.view.App";
			},

			/**
			 * Is initially called once after the Controller has been
			 * instantiated. It is the place where the UI is constructed. Since
			 * the Controller is given to this method, its event handlers can be
			 * attached right away.
			 * 
			 * @memberOf view.App
			 */
			createContent : function(oController) {

				this.setDisplayBlock(true);
				var splitApp = new sap.m.SplitApp("splitAppId");
				var master = new sap.ui.xmlview("masterId",
						"EnYapp.view.Master");
				var detail = new sap.ui.xmlview("detailId",
						"EnYapp.view.Detail");
				var ganttChartExpandId = new sap.ui.xmlview("ganttChartExpandId",
				"EnYapp.view.ganttChartExpand");
				sap.ui.getCore().byId("splitAppId-Master").addStyleClass("masterPageWidth");
				
				splitApp.addMasterPage(master);
				splitApp.addDetailPage(detail);
				splitApp.addDetailPage(ganttChartExpandId);
				return splitApp;
			}

		});