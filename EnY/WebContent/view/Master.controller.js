var footerState= false;
sap.ui.controller("EnYapp.view.Master", {

	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 * 
	 * @memberOf view.Master
	 */
	onInit : function() {
		/*if(!sap.ui.Device.system.phone){
		var oList = this.getView().byId("list");
			 this.getView().byId("list") oList.attachEvent("updateFinished",
					function() {
						var items = oList.getItems();
						oList.setSelectedItem(items[0]);
						oList.fireSelect({
							"listItem" : items[0]
						});
	
					} this );
		 }*/
		
		/*sap.ui.getCore().byId("detailId--footerHide").addStyleClass("footerHide");
		sap.ui.getCore().byId("detailId--footerFormHide").addStyleClass("footerHide");
		footerState= false;*/
	},
	/**
	 * Similar to onAfterRendering, but this hook is invoked before the
	 * controller's View is re-rendered (NOT before the first rendering!
	 * onInit() is used for that one!).
	 * 
	 * @memberOf view.Master
	 */
	// onBeforeRendering: function() {
	//
	// },
	/**
	 * Called when the View has been rendered (so its HTML is part of the
	 * document). Post-rendering manipulations of the HTML could be done here.
	 * This hook is the same one that SAPUI5 controls get after being rendered.
	 * 
	 * @memberOf view.Master
	 */
	// onAfterRendering: function() {
	//
	// },
	/**
	 * Called when the Controller is destroyed. Use this one to free resources
	 * and finalize activities.
	 * 
	 * @memberOf view.Master
	 */
	// onExit: function() {
	//
	// }

	onFilterInvoices : function() {
		jQuery.sap.require("sap.ui.model.Filter");
		jQuery.sap.require("sap.ui.model.FilterOperator");

		// build filter array
		var aFilter = [];
		// var sQuery = oEvent.getParameter("query");
		var sQuery = this.getView().byId("search").getValue();
		if (sQuery) {
			aFilter = [ new sap.ui.model.Filter("FirstName",
					sap.ui.model.FilterOperator.Contains, sQuery) ];
		}

		// filter binding
		var oList = this.getView().byId("list").getBinding("items").filter(
				aFilter);// invoiceList is id of
		// table
		// items is items attribute
		// of table tag

	},
	hide: function(){
		var rejectBtn = sap.ui.getCore().byId("splitAppId-Master");
	        if(rejectBtn.getVisible()) {
	           rejectBtn.setVisible(false);
	       }
	},
	handleNext: function(){
		sap.ui.getCore().byId("splitAppId").toDetail("ganttChartExpandId");
	},
	footerButton: function(){
		/*var data = sap.ui.getCore().getModel("jsonData");
		console.log(data);*/

		if(footerState){
			footerState=false;
		sap.ui.getCore().byId("detailId--footerHide").removeStyleClass("footerShow");
		sap.ui.getCore().byId("detailId--footerFormHide").removeStyleClass("footerShow");
		sap.ui.getCore().byId("detailId--footerHide").addStyleClass("footerHide");
		sap.ui.getCore().byId("detailId--footerFormHide").addStyleClass("footerHide");
		}
		else{
			footerState=true;
			sap.ui.getCore().byId("detailId--footerHide").removeStyleClass("footerHide");
			sap.ui.getCore().byId("detailId--footerHide").addStyleClass("footerShow");
			sap.ui.getCore().byId("detailId--footerFormHide").removeStyleClass("footerHide");
			
			sap.ui.getCore().byId("detailId--footerFormHide").addStyleClass("footerShow");
		}
	}
});