sap.ui.jsfragment("EnYapp.view.ganttChartExpand", {
	createContent : function(oController) {
				/*google.setOnLoadCallback(Chart);*/
				function toMilliseconds(minutes) {
					return minutes * 60 * 1000;
				}

				/*function Chart() {*/
					var empJSONObj = sap.ui.getCore().getModel("jsonData");
					
					var data = new google.visualization.DataTable();
					data.addColumn('string', 'Task ID');
					data.addColumn('string', 'Task Name');
					data.addColumn('string', 'Resource');
					data.addColumn('date', 'Start Date');
					data.addColumn('date', 'End Date');
					data.addColumn('number', 'Duration');
					data.addColumn('number', 'Percent Complete');
					data.addColumn('string', 'Dependencies');
					for (var i = 0; i < empJSONObj.oData.task.length; i++) {
						data.addRows([ [ empJSONObj.oData.task[i].TaskID,
								empJSONObj.oData.task[i].TaskName,
								empJSONObj.oData.task[i].Resource,
								new Date(empJSONObj.oData.task[i].StartDate),
								new Date(empJSONObj.oData.task[i].EndDate),
								parseInt(empJSONObj.oData.task[i].Duration),
								parseInt(empJSONObj.oData.task[i].PercentComplete),
								empJSONObj.oData.task[i].Dependencies ] ]);

					}
					
				     /* var options = {
				        height: 400,
				         gantt: { criticalPathEnabled: false, arrow: {
				              angle: 100,
				              width: 5,
				              color: 'green',
				              radius: 0
						 
				          
						//trackHeight: 30
				        }
						
						  }
				      };*/
					var options = {
						height : '100%',
						width: '100%',
						gantt : {
							criticalPathEnabled : false,

							arrow : {
								angle : 100,
								width : 5,
								color : 'red',
								radius : 0
							// defaultStartDateMillis: new Date(2015, 3, 28)
							}
						}
					};
					var options1 = {
						height : 275,
						gantt : {
							criticalPathEnabled : false,

							arrow : {
								angle : 100,
								width : 5,
								color : 'red',
								radius : 0
							// defaultStartDateMillis: new Date(2015, 3, 28)
							}
						}
					};
					var chart = new google.visualization.GanttChart(document
							.getElementById('__panel12'));
					/*var containerSize = $("#ganttChartExpandId").width();
					 alert(containerSize) 
					if (containerSize > 960) {
						chart.draw(data, options);
					} else if (containerSize <= 959) {
						chart.draw(data, options1);
					}

					function resizeHandler() {
						chart.draw(data, options1);
					}
					if (window.addEventListener) {
						window.addEventListener('resize', resizeHandler, false);
						window.addEventListener('load', resizeHandler, false);
					} else if (window.attachEvent) {
						window.attachEvent('onresize', resizeHandler);
						window.attachEvent('onload', resizeHandler);
					}
*/
					chart.draw(data, options);
				/*}*/

			}

		});
	