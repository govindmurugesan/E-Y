sap.ui.jsfragment("EnYapp.view.ganttChart", {
	createContent : function(oController) {

		google.setOnLoadCallback(drawChart);
		function toMilliseconds(minutes) {
			return minutes * 60 * 1000;
		}

		function drawChart() {
			/*
			 * var otherData = new google.visualization.DataTable();
			 * otherData.addColumn('string', 'Task ID');
			 * otherData.addColumn('string', 'Task Name');
			 * otherData.addColumn('string', 'Resource');
			 * otherData.addColumn('date', 'Start'); otherData.addColumn('date',
			 * 'End'); otherData.addColumn('number', 'Duration');
			 * otherData.addColumn('number', 'Percent Complete');
			 * otherData.addColumn('string', 'Dependencies');
			 * 
			 * otherData.addRows([ ['toTrain', 'Walk to train stop', 'walk',
			 * null, null, toMilliseconds(5), 100, null], ['music', 'Listen to
			 * music', 'music', null, null, toMilliseconds(70), 100, null],
			 * ['wait', 'Wait for train', 'wait', null, null,
			 * toMilliseconds(10), 100, 'toTrain'], ['train', 'Train ride',
			 * 'train', null, null, toMilliseconds(45), 75, 'wait'], ['toWork',
			 * 'Walk to work', 'walk', null, null, toMilliseconds(10), 0,
			 * 'train'], ['work', 'Sit down at desk', null, null, null,
			 * toMilliseconds(2), 0, 'toWork'],
			 * 
			 * ]);
			 */
			var jsonObject = {
					"task": [
					 		{         
					             "TaskID": "2014Spring",
					             "TaskName": "Spring 2014",
					             "Resource": "spring",
					             "StartDate": "2014, 2, 22",
					             "EndDate": "2014, 5, 20",
					 			"Duration":"",
					 			"PercentComplete":"100",
					 			"Dependencies":""
					           },
					 		  
					           {
					              "TaskID": "2013Spring",
					             "TaskName": "Spring 2015",
					             "Resource": "spring1",
					             "StartDate": "2013, 1, 27",
					             "EndDate": "2015, 4, 21",
					 			"Duration":"",
					 			"PercentComplete":"90",
					 			"Dependencies":""
					           
					           },
					 		  
					           {
					              "TaskID": "2012Spring",
					             "TaskName": "Spring 2013",
					             "Resource": "spring2",
					             "StartDate": "2012, 3, 26",
					             "EndDate": "2014, 5, 20",
					 			"Duration":"",
					 			"PercentComplete":"80",
					 			"Dependencies":""
					           
					           },
					 		  
					           {
					              "TaskID": "2016Spring",
					             "TaskName": "Spring 2016",
					             "Resource": "spring3",
					             "StartDate": "2014, 2, 22",
					             "EndDate": "2014, 5, 20",
					 			"Duration":"",
					 			"PercentComplete":"99",
					 			"Dependencies":""
					           },
					 		  
					 		  {
					              "TaskID": "2017Spring",
					             "TaskName": "Spring 2016",
					             "Resource": "spring4",
					             "StartDate": "2014, 2, 22",
					             "EndDate": "2014, 5, 20",
					 			"Duration":"",
					 			"PercentComplete":"99",
					 			"Dependencies":""
					           },
					           {
						              "TaskID": "2018Spring",
						             "TaskName": "Spring 2016",
						             "Resource": "spring4",
						             "StartDate": "2014, 2, 22",
						             "EndDate": "2014, 5, 20",
						 			"Duration":"",
						 			"PercentComplete":"99",
						 			"Dependencies":""
						           },
						           {
							              "TaskID": "2019Spring",
							             "TaskName": "Spring 2016",
							             "Resource": "spring4",
							             "StartDate": "2014, 2, 22",
							             "EndDate": "2014, 5, 20",
							 			"Duration":"",
							 			"PercentComplete":"99",
							 			"Dependencies":""
							           },
							           {
								              "TaskID": "2020Spring",
								             "TaskName": "Spring 2016",
								             "Resource": "spring4",
								             "StartDate": "2014, 2, 22",
								             "EndDate": "2014, 5, 20",
								 			"Duration":"",
								 			"PercentComplete":"99",
								 			"Dependencies":""
								           }
					           
					         ]
			}

			var empJSONObj = new sap.ui.model.json.JSONModel();
			empJSONObj.setData(jsonObject);
			sap.ui.getCore().setModel(empJSONObj,"jsonData");
			console.log(empJSONObj);
			var data = new google.visualization.DataTable("","","");
			data.addColumn('string', 'Task ID');
			data.addColumn('string', 'Task Name');
			data.addColumn('string', 'Resource');
			data.addColumn('date', 'Start Date');
			data.addColumn('date', 'End Date');
			data.addColumn('number', 'Duration');
			data.addColumn('number', 'Percent Complete');
			data.addColumn('string', 'Dependencies');
			for (var i = 0; i < empJSONObj.oData.task.length; i++) {
				data.addRows([ [ empJSONObj.oData.task[i].TaskID,
						empJSONObj.oData.task[i].TaskName,
						empJSONObj.oData.task[i].Resource,
						new Date(empJSONObj.oData.task[i].StartDate),
						new Date(empJSONObj.oData.task[i].EndDate),
						parseInt(empJSONObj.oData.task[i].Duration),
						parseInt(empJSONObj.oData.task[i].PercentComplete),
						empJSONObj.oData.task[i].Dependencies ] ]);

			}
			
		     /* var options = {
		        height: 400,
		         gantt: { criticalPathEnabled: false, arrow: {
		              angle: 100,
		              width: 5,
		              color: 'green',
		              radius: 0
				 
		          
				//trackHeight: 30
		        }
				
				  }
		      };*/
			var options = {
				height : 275,
				width: '75%',
				gantt : {
					criticalPathEnabled : false,

					arrow : {
						angle : 100,
						width : 5,
						color : 'red',
						radius : 0
					// defaultStartDateMillis: new Date(2015, 3, 28)
					}
				}
			};
			var options1 = {
				height : 275,
				width : '50%',
				gantt : {
					criticalPathEnabled : false,

					arrow : {
						angle : 100,
						width : 5,
						color : 'red',
						radius : 0
					// defaultStartDateMillis: new Date(2015, 3, 28)
					}
				}
			};
			var chart = new google.visualization.GanttChart(document
					.getElementById('detailId--ganttChart'));
			var containerSize = $("#detailId--ganttChart").width();
			/* alert(containerSize) */
			if (containerSize > 960) {
				chart.draw(data, options);
			} else if (containerSize <= 959) {
				chart.draw(data, options1);
			}

			function resizeHandler() {
				chart.draw(data, options1);
			}
			if (window.addEventListener) {
				window.addEventListener('resize', resizeHandler, false);
				window.addEventListener('load', resizeHandler, false);
			} else if (window.attachEvent) {
				window.attachEvent('onresize', resizeHandler);
				window.attachEvent('onload', resizeHandler);
			}

			chart.draw(data, options);
		}

	}

});