sap.ui.controller("EnYapp.view.ganttChartExpand", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf view.ganttChartExpand
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf view.ganttChartExpand
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf view.ganttChartExpand
*/
	onAfterRendering: function() {
		sap.ui.jsfragment("EnYapp.view.ganttChartExpand");
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf view.ganttChartExpand
*/
//	onExit: function() {
//
//	}
	menuButton: function(){
		/*var data = sap.ui.getCore().getModel("jsonData");
		console.log(data);*/
		if(menuState){
			menuState=false;
		sap.ui.getCore().byId("splitAppId-Master").removeStyleClass("menuShow");
		sap.ui.getCore().byId("splitAppId-Master").addStyleClass("menuHide");
		}
		else{
			menuState=true;
		sap.ui.getCore().byId("splitAppId-Master").removeStyleClass("menuHide");
		sap.ui.getCore().byId("splitAppId-Master").addStyleClass("menuShow");
		}
	}
});